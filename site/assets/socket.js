// functions for websockets

// connect to the socket server
const hostname = window.location.hostname;
const port = 32090;

let ws = null;

function connect() {
  ws = new WebSocket("ws://" + hostname + ":" + port);
  ws.onopen = function () {
    console.log("Connected to socket server");
    document.getElementById("conn_status").innerText = "OK";
  };

  ws.onmessage = function (e) {
    console.log("Message:", e.data);
  };

  ws.onclose = function (e) {
    console.log(
      "Socket is closed. Reconnect will be attempted in 1 second.",
      e.reason
    );
    document.getElementById("conn_status").innerText = "X";
    setTimeout(function () {
      connect();
    }, 1000);
  };

  ws.onerror = function (err) {
    console.log("Socket encountered error: ", err.message, "Closing socket");
    ws.close();
  };
}

// Function to check if WebSocket is connected
function isConnected() {
  return ws && ws.readyState === WebSocket.OPEN;
}

// send joystick data to the server
function sendJoystickData(data) {
  if (isConnected()) {
    // console.log("sending:", data);
    ws.send(data);
  }
}

// connect initially
connect();
