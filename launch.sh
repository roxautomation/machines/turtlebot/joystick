#!/bin/bash

# launc docker image, autorestart

IMG=roxauto/joystick:latest
docker pull $IMG
docker run -d --restart unless-stopped -p 6001:80 $IMG
