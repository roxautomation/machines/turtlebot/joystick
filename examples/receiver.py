#!/usr/bin/env python3
"""
 example of a wesocket server that recieves data from joystick

 Copyright (c) 2023 ROX Automation - Jev Kuznetsov
"""

import asyncio
import websockets

PORT = 32090


async def receiver(websocket):
    async for message in websocket:
        print(message)


async def main():
    print(f"Listening on port {PORT}")
    print("press Ctrl-C to exit")

    async with websockets.serve(receiver, "0.0.0.0", PORT):  # pylint: disable=no-member
        await asyncio.Future()  # run forever


if __name__ == "__main__":
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        pass
