FROM httpd:alpine

ENV HTDOCS=/usr/local/apache2/htdocs


COPY site/ ${HTDOCS}
