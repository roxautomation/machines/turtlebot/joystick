#!/bin/bash

PORT=6001

echo "Starting server on port $PORT"

python -m http.server $PORT --directory ./site
